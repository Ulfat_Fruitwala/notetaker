# Notetaker
-----------
Notetaker is a web application that collects and organizes all of your notes in one place.

## Getting Started
------------------
These instructions will get you a copy of the project up and running on your local machine for development and testing.

Prerequisites
-------------
-Python 3

-PY4WEB: Installing from pip
```bash
py4web setup apps
py4web set-password
py4web run apps
```
After installing py4web, your application can be accessed at the following urls:
```bash
http://localhost:8000
http://localhost:8000/_dashboard
http://localhost:8000/{yourappname}/index
```

Installing
----------
Go to the apps folder in your py4web directory on your local machine
```bash
cd py4web/apps
git clone https://Ulfat_Fruitwala@bitbucket.org/Ulfat_Fruitwala/notetaker.git
```
The application can be accessed at the following url
```bash
http://localhost:8000/notetaker/index
```

## Usage
--------
- Sign up for an account accordingly
- Press the plus sign for adding a new note
- To save a note, simply move your cursor out of the note and the note will be saved
- Important notes can be starred; a starred note will appear before unstarred notes
- There are 5 note colours to choose from
- To delete a note, simply click on the delete button at the bottom of the note you wish to delete

## Built Using
--------------
- py4web
- vue.js
- python
- javascript
- html/css

## Author
---------
- Ulfat Fruitwala